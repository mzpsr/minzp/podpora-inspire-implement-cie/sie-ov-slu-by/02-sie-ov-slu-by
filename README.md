# 02 Sieťové služby

Diskusný priestor pre problematiku podpory tvorby, správy a využívania sieťových služieb. Jednotlivé príspevky je možné vytvárať [v časti Issues](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/02-sie-ov-slu-by/issues).
Dokumentácia príkladov dobrej praxe a výsledkov diskusií je k dispozícii na [Wiki Dobrej praxe](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/02-sie-ov-slu-by/wikis/Dobr%C3%A1-prax-pre-sie%C5%A5ov%C3%A9-slu%C5%BEby).